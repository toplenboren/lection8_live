import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object MonadInstances {

  implicit val futureMonad: Monad[Future] = new Monad[Future] {
    override def pure[A](a: => A): Future[A] = Future(a)
    override def flatMap[A, B](fa: Future[A])(f: A => Future[B]): Future[B] = fa.flatMap(f)
  }

  implicit val optionMonad: Monad[Option] = new Monad[Option] {
    override def pure[A](a: => A): Option[A] = Some(a)
    override def flatMap[A, B](fa: Option[A])(f: A => Option[B]): Option[B] = fa match {
      case None    => None
      case Some(x) => f(x)
    }
  }

  implicit val listMonad = new Monad[List] {
    override def pure[A](a: => A): List[A] = a :: Nil
    override def flatMap[A, B](fa: List[A])(f: A => List[B]): List[B] = fa.flatMap(f)
  }

  implicit val writerMonad = new Monad[Writer] {
    override def pure[A](a: => A): Writer[A] = Writer(a, Nil)
    override def flatMap[A, B](fa: Writer[A])(f: A => Writer[B]): Writer[B] = {
      f(fa.value) match {
        case Writer(b, logs) => Writer(b, fa.logs ++ logs)
      }
    }
  }

}
