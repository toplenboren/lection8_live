import scala.concurrent.Future

object Main extends App {
  import Monad._
  import MonadInstances._

  val f: Int => Option[String] = x =>
    if (x % 2 == 0) Some(x.toString)
    else None

  val g: String => Option[Int] = x =>
    if (x.length % 2 == 0) Some(x.length)
    else None

  val h: Int => Option[Boolean] = x =>
    if (x > 5) Some(true)
    else if (x > 0) Some(false)
    else None

  val a = pure[Option, Int] _ >=> f


  println("Hello world")

}