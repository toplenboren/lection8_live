object Step05State extends App {
  import Monad._

  case class State[S, A](run: S => (A, S))

  implicit def stateMonad[S]: Monad[State[S, *]] = new Monad[State[S, *]] {
    override def pure[A](a: => A): State[S, A] = State(s => (a, s))
    override def flatMap[A, B](fa: State[S, A])(f: A => State[S, B]): State[S, B] = State { s =>
      val (value, st) = fa.run(s)
      f(value).run(st)
    }
  }

  def modify[S](f: S => S): State[S, Unit] = State(s => ((), f(s)))
  def get[S]: State[S, S] = State(s => (s, s))

  val init = pure[State[List[Int], *], String]("poopa")

  val b = for {
    i <- init
    _ <- modify[List[Int]](_.take(2))
    k <- get[List[Int]]
    _  = println(k)
    _ <- modify[List[Int]](3 :: 4 :: _)
    z <- get[List[Int]]
    _  = println(z)
  } yield i

  println(b.run(1 :: 2 :: 3 :: Nil))

}
