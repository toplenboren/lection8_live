import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object Step07Transformers extends App {
  import Monad._
  import MonadInstances._

//  val futureOption: Future[Option[String]] = ???
//  val work  = futureOption.map(_.map(_.length))
//  val work2 = work.map(_.map(_.toString))

  case class OptionT[F[_], A](value: F[Option[A]])

  implicit def optionTMonad[F[_]](implicit mf: Monad[F]) = new Monad[OptionT[F, *]] {
    override def pure[A](a: => A): OptionT[F, A] = OptionT(mf.pure(Some(a)))

    override def flatMap[A, B](fa: OptionT[F, A])(f: A => OptionT[F, B]): OptionT[F, B] = OptionT {
      mf.flatMap(fa.value) {opt => opt match {
          case None    => mf.pure[Option[B]](None)
          case Some(x) => f(x).value
        }
      }
    }
  }

  val initF = pure[OptionT[Future, *], String]("kek") // OptionT[Future, String](value: Future(Some("kek")))

  val testF = for {
    i <- initF
    _ <- OptionT(Future[Option[String]](None))
    z <- OptionT(Future[Option[String]] { Some("lol") })
    _  = println(z)
  } yield i ++ z

  Thread.sleep(200)
  println(testF)

  /*
  val testL = for {
    j <- OptionT(List(Some("kek"), None, Some("lal")))
    i <- pure[OptionT[List, *], String]("azaza")
  } yield j + i

  println(testL)
   */


}
