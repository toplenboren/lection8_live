object Step01MonadLaws extends App {
  import Monad._
  import MonadInstances._

  val f: Int => Option[String] = ???

  val g: String => Option[Int] = ???

  val h: Int => Option[Boolean] = ???

  /*
     1. Left identity law
  */

//  pure[Option, Int] _ >=> f == f

  /*
     2. Right identity law
  */

//  f >=> pure == f

  /*
     3. Associativity composition law
  */

//  (f >=> g) >=> h == f >=> (g >=> h)

}
