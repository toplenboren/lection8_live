object Step03Reader {

  case class Reader[Env, B](run: Env => B)

  /*
  def readerMonad[Env]: Monad[({type Help[X] = Reader[Env, X]})#Help] = new Monad[({type Help[X] = Reader[Env, X]})#Help] {
    override def pure[A](a: => A): Reader[Env, A] = Reader(_ => a)

    override def flatMap[A, B](fa: Reader[Env, A])(f: A => Reader[Env, B]): Reader[Env, B] = Reader {e =>
      (fa.run andThen f)(e).run(e)
    }
  }

   */

  implicit def readerMonad2[Env]: Monad[Reader[Env, *]] = new Monad[Reader[Env, *]] {

    override def pure[A](a: => A): Reader[Env, A] = Reader(_ => a)

    override def flatMap[A, B](fa: Reader[Env, A])(f: A => Reader[Env, B]): Reader[Env, B] = Reader {e =>
      (fa.run andThen f)(e).run(e)
    }
  }

}
