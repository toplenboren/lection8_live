
trait Monad[F[_]] {
  def pure[A](a: => A): F[A]
  def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B]

  def map[A, B](fa: F[A])(f: A => B): F[B] =
    flatMap(fa)(a => pure(f(a)))
}

object Monad {
  def apply[F[_]](implicit mf: Monad[F]) = mf

  implicit class MonadSyntax[F[_] : Monad, A](fa: F[A]) {
    def flatMap[B](f: A => F[B]) = Monad[F].flatMap(fa)(f)
    def map[B](f: A => B) = Monad[F].map(fa)(f)
  }

  implicit class SyntaxE[F[_] : Monad, A, B](f: A => F[B]) {
    def >=>[C](g: B => F[C]): A => F[C] = a => Monad[F].flatMap(f(a))(g)
  }

  def pure[F[_], A](a: A)(implicit mf: Monad[F]) = mf.pure[A](a)

}
