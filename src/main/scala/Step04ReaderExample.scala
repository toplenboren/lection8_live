import Step03Reader.Reader

object Step04ReaderExample extends App {
  import Monad._
  import Step03Reader._

  implicit class ReaderExt[A, B](r: Reader[A, B]) {
    def local[AA](f: AA => A): Reader[AA, B] = Reader(f.andThen(r.run))
  }

  trait Database
  object Database {
    case class DbConfig(url: String, password: String)
    def fromConfig: Reader[DbConfig, Database] = Reader(_ => new Database {})
  }

  trait Service
  object Service {
    case class ServiceConfig(url: String, password: String)
    def fromConfig: Reader[ServiceConfig, Service] = Reader(_ => new Service {})
  }

  case class AppConfig(db: Database.DbConfig, srvc: Service.ServiceConfig)

  case class App(db: Database, srvc: Service)

  val initApp = for {
    db   <- Database.fromConfig.local[AppConfig](_.db)
    srvc <- Service.fromConfig.local[AppConfig](_.srvc)
  } yield App(db, srvc)

  println(initApp.run(AppConfig(Database.DbConfig("url", "password"), Service.ServiceConfig("url", "password"))))
}
