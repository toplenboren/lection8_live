object Step09Identity extends App {

  import Step07Transformers.OptionT
  import Step08ReaderT._

  type Id[T] = T

  implicit val identityMonad = new Monad[Id] {
    override def pure[A](a: => A): Id[A] = a
    override def flatMap[A, B](fa: Id[A])(f: A => Id[B]): Id[B] = f(fa)
  }

  type Reader[Env, B] = ReaderT[Id, Env, B]

  type Option[T] = OptionT[Id, T]

}
